;;TODO
;http://stackoverflow.com/questions/683425/globally-override-key-binding-in-emacs/1758639#1758639


;; set font
(set-default-font "Inconsolata-dz-10")

;; Saved files
(setq
   backup-by-copying t 
   backup-directory-alist '(("." . "~/.emacs.d/saves"))
   delete-old-versions t
   kept-new-versions 6
   kept-old-versions 2
   version-control t
)

;; Scroll bar
(set-scroll-bar-mode 'right)



(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))


;; Movement key bindings
(global-set-key (kbd "C-j") 'backward-word)
(global-set-key (kbd "C-f") 'forward-word)

(global-set-key (kbd "C-i") 'backward-char)
(global-set-key (kbd "C-e") 'forward-char)

(global-set-key (kbd "C-k") 'previous-line)
(global-set-key (kbd "C-d") 'next-line)
(define-key global-map (kbd "C-d") 'next-line) 

(global-set-key (kbd "C-o") 'move-beginning-of-line)
(global-set-key (kbd "C-w") 'move-end-of-line)

(global-set-key (kbd "C-s") 'scroll-up)
(global-set-key (kbd "C-l") 'scroll-down)

(global-set-key [tab] 'indent-for-tab-command)
(define-key minibuffer-local-completion-map [tab] 'minibuffer-complete)
(global-set-key (kbd "C-;") 'recenter-top-bottom)


;; isearch
(global-set-key (kbd "C-a") 'isearch-forward)
(global-set-key (kbd "C-q") 'isearch-backward)
(define-key isearch-mode-map (kbd "C-a") 'isearch-repeat-forward)
(define-key isearch-mode-map (kbd "C-q") 'isearch-repeat-backward)
(define-key isearch-mode-map (kbd "<backspace>") 'isearch-delete-char)
(define-key isearch-mode-map (kbd "<delete>") 'isearch-del-char)

(global-set-key (kbd "C-p") 'goto-line)



;; Text Manipulation
(global-set-key (kbd "C-n") 'kill-line) 
(global-set-key (kbd "C-m") 'kill-ring-save)
(global-set-key (kbd "C-S-n") 'yank)
(global-set-key (kbd "C-S-m") 'yank-pop)
(global-set-key (kbd "C-<return>") 'c-return)
(global-set-key (kbd "RET") 'newline)
(global-set-key (kbd "C-<backspace>") 'backward-kill-word)
(global-set-key (kbd "<backspace>") 'backward-delete-char)
(global-set-key (kbd "<delete>") 'delete-char)
(global-set-key (kbd "C-<delete>") 'kill-word)
(global-set-key (kbd "M-N") 'clipboard-yank)
(global-set-key (kbd "M-M") 'clipboard-kill-ring-save)

(show-paren-mode 1) ; match matching paren

(defun c-return() (interactive) (move-end-of-line nil) (newline-and-indent)) ; for "C-<return>"

;; Auto complete
(global-set-key (kbd "C-SPC") 'dabbrev-expand)



;; stop starup message from popping up
(setq inhibit-startup-message t)


;; auto-complete buffer
(require 'ido)
(ido-mode t)
(setq ido-enable-flex-matching t) ; fuzzy matching

;; end


;; Location of various emacs files

; Location of color-themes
(add-to-list 'load-path "~/.emacs.d/color_themes/")
(require 'color-theme)
;(color-theme-initialize)
(color-theme-andreas)